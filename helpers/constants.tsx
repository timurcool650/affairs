import {useState} from 'react';
import {Alert} from "react-native";

let token = '';
let user = {};

export function SetToken(t: string){
    token = t;
    console.log(token);
}
export function SetUser(u: any){
    user = u;
    console.log(user);
}


const api = `http://affairs.webeasy.kz/api`;

export async function _POST(url: string, bodyArray: any)
{
    const API_URL = `${api}/${url}`

    if (token === null) {
        // @ts-ignore
        //setLoginForm();
        return null;
    }

    try {
        let h1 = (token !== '') ? {"Autorization" : token} : {};
        let header = {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            h1
        };

        const response = await fetch(API_URL, {
            method: 'POST',
            // @ts-ignore
            headers: header,
            body: JSON.stringify(bodyArray)
        });

        const responseJson = await response.json();

        if (responseJson !== null) {
            if (responseJson.success === false) {
                Alert.alert(responseJson.message);
                return null;
            }
            if(responseJson.message !== ''){
                Alert.alert(responseJson.message);
            }
            return ('result' in responseJson) ? responseJson.result : responseJson;
            //return (res == null) ? true : res;
        }
    } catch (error) {
        Alert.alert("Warning error: " + error.message);
    }
    return null;
}

export async function _GET(url: string)
{
    const API_URL = `${api}/${url}`

    if (token === '') {
        // @ts-ignore
        return null;
    }

    try {
        // eslint-disable-next-line no-undef
        const response = await fetch(API_URL, {
            method: 'GET',
            // @ts-ignore
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': token,
            },
        });

        const responseJson = await response.json();

        if (responseJson !== null) {
            if (responseJson.success === false) {
                Alert.alert(responseJson.message);
                return null;
            }
            if(responseJson.message !== ''){
                Alert.alert(responseJson.message, responseJson.success);
            }
            return ('result' in responseJson) ? responseJson.result : responseJson;
        }
    } catch (error) {
        Alert.alert("Warning error: " + error.message);
    }
    return null;
}