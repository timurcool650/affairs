import React, { useState } from "react";
import {StyleSheet, Text, TouchableOpacity, View} from "react-native";

function Main({ navigation }) {
    return (
        <View>
            <TouchableOpacity style={styles.add} onPress={() => {navigation.navigate('AffairsCreate')}}>
                <Text style={styles.text}>Добавить</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    add: {
        alignItems: 'center',
        backgroundColor: '#f7a474',
        padding: 25,
        width: '90%',
        marginTop: 600,
        marginLeft: '5%',
        borderRadius: 40,
    },
    text: {
        fontWeight: "bold",
        fontSize: 15,
    },

})


export default Main;

