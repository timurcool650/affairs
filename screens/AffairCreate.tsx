import React, { useState } from "react";
import {FlatList, StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";

function AffairsCreate({ navigation }) {
    return (
        <View>
            <TextInput
                style={styles.input}
                placeholder={"Введите название задачи"}
            />
            <TextInput
                style={styles.input}
                placeholder={"Введите описание задачи"}
            />
            <TextInput
                style={styles.input}
                placeholder={"Введите срок сдачи"}
                keyboardType={"number-pad"}
            />
            <TouchableOpacity style={styles.add} onPress={() => {navigation.navigate('Главная страница')}}>
                <Text style={styles.text}>Добавить</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.back} onPress={() => {navigation.navigate('Главная страница')}}>
                <Text style={styles.text}>Вернуться</Text>
            </TouchableOpacity>
        </View>
    );
};

const styles = StyleSheet.create({
    add: {
        alignItems: 'center',
        backgroundColor: '#f7a474',
        padding: 25,
        width: '90%',
        marginTop: 50,
        marginLeft: '5%',
        borderRadius: 40,
    },
    back: {
        alignItems: 'center',
        backgroundColor: '#5bafd4',
        padding: 25,
        width: '90%',
        marginTop: 50,
        marginLeft: '5%',
        borderRadius: 40,
    },
    text: {
        fontWeight: "bold",
        fontSize: 15,
    },
    input: {
        borderStyle: "solid",
        borderWidth: 2,
        borderColor: "#FFDEAD",
        width: "90%",
        marginLeft: 20,
        padding: 5,
        height: 50,
        marginTop: 30,
        fontStyle: "italic"
    },

})


export default AffairsCreate;