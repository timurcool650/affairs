import React, {useState} from "react";
import {StyleSheet, Text, TextInput, TouchableOpacity, View} from "react-native";
import {_POST, SetToken, SetUser} from "../helpers/constants";

// @ts-ignore
function Regist({navigation}) {
    const [name, setName] = useState('');
    const [login, setLogin] = useState('');
    const [password, setPassword] = useState('');

    const sendRegistration = async () => {
        _POST('registration', {
            "name": name,
            "email": login,
            "password": password
        }).then(res => {
            if(res !== null){
                SetToken(res.token);
                SetUser(res.user);
                navigation.navigate('Main');
            }
        })
    }

    return(
        <View>
            <TextInput
                style={styles.name}
                onChangeText={text => setName(text)}
                value={name}
                placeholder={"Введите Имя"}
            />
            <TextInput
                style={styles.input}
                onChangeText={text => setLogin(text)}
                value={login}
                placeholder={"Введите логин"}
            />
            <TextInput secureTextEntry={true}
                style={styles.pass}
                onChangeText={text => setPassword(text)}
                value={password}
                placeholder={"Введите пароль"}
            />
            <TouchableOpacity style={styles.button} onPress={sendRegistration}>
                <Text>Зарегистрироваться</Text>
            </TouchableOpacity>
        </View>
    );
};




const styles = StyleSheet.create({
    name: {
        borderStyle: "solid",
        borderWidth: 2,
        borderColor: "#FFDEAD",
        width: "90%",
        marginLeft: 20,
        padding: 5,
        height: 35,
        marginTop: 30,
        fontStyle: "italic"
    },
    input : {
        borderStyle: "solid",
        borderWidth: 2,
        borderColor: "#FFDEAD",
        width: "90%",
        marginLeft: 20,
        padding: 5,
        height: 35,
        marginTop: 20,
        fontStyle: "italic"
    },
    pass : {
        borderStyle: "solid",
        borderWidth: 2,
        borderColor: "#FFDEAD",
        width: "90%",
        marginLeft: 20,
        padding: 5,
        height: 35,
        marginTop: 20,
        fontStyle: "italic"
    },
    button: {
        alignItems: 'center',
        padding: 25,
        width: '90%',
        marginTop: 30,
        marginLeft: '5%',
        backgroundColor: '#E6E6FA',
        borderRadius: 40,
    },
})

export default Regist;



