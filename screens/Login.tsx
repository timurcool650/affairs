import React, { useState } from "react";
import { StyleSheet, Text, TextInput, TouchableOpacity, View } from "react-native";
import {_POST, SetToken, SetUser} from "../helpers/constants";

// @ts-ignore
function Login({ navigation }) {
    const [login, setLogin] = useState('admin@mail.ru');
    const [password, setPassword] = useState('12345678');

    const SendLogin = async () => {
        _POST('login', {"email": login, "password": password}).then(res => {
            if(res !== null){
                SetToken(res.token);
                SetUser(res.user);
                navigation.navigate('Main');
            }
        })
    }

    return (
        <View>
            <TextInput
                style={styles.input}
                onChangeText={text => setLogin(text)}
                value={login}
                placeholder={"Введите логин"}
            />
            <TextInput secureTextEntry={true}
                style={styles.pass}
                onChangeText={text => setPassword(text)}
                value={password}
                placeholder={"Введите пароль"}
            />
            <TouchableOpacity style={styles.button} onPress={SendLogin}>
                <Text>Авторизоваться</Text>
            </TouchableOpacity>
            <TouchableOpacity style={styles.butreg} onPress={() => { navigation.navigate('Regist'); }}>
                <Text> Регистрация</Text>
            </TouchableOpacity>
        </View>
    );
};


const styles = StyleSheet.create({
    input: {
        borderStyle: "solid",
        borderWidth: 2,
        borderColor: "#FFDEAD",
        width: "90%",
        marginLeft: 20,
        padding: 5,
        height: 35,
        marginTop: 30,
        fontStyle: "italic"

    },
    pass: {
        borderStyle: "solid",
        borderWidth: 2,
        borderColor: "#FFDEAD",
        width: "90%",
        marginLeft: 20,
        padding: 5,
        height: 35,
        marginTop: 25,
        fontStyle: "italic"
    },
    container: {
        flex: 1,
        justifyContent: 'center',
        paddingHorizontal: 40,
    },
    button: {
        alignItems: 'center',
        padding: 25,
        width: '90%',
        marginTop: 40,
        marginLeft: '5%',
        backgroundColor: "#FFDEAD",
        borderRadius: 40,

    },
    butreg: {
        alignItems: 'center',
        padding: 25,
        width: '90%',
        marginTop: 40,
        marginLeft: '5%',
        backgroundColor: '#E6E6FA',
        borderRadius: 40,
    }
})

export default Login;