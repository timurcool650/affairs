import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import React from 'react';

import Login from "../screens/Login";
import Regist from "../screens/Registration";
import Main from "../screens/Main";
import AffairsCreate from "../screens/AffairCreate";
const Stack = createNativeStackNavigator();

function Nav() {
    return (
        <NavigationContainer>
            <Stack.Navigator initialRouteName={"Login"}>
                <Stack.Screen name="Login" component={Login} options={{
                    title : "Вход"
                }}/>
                <Stack.Screen name="Regist" component={Regist} options={{
                    title : "Регистрация"
                }}/>
                <Stack.Screen name="Main" component={Main} options={{
                    title : "Главная страница",
                }}/>
                <Stack.Screen name="AffairsCreate" component={AffairsCreate} options={{
                    title : "Создание задач"
                }}/>
            </Stack.Navigator>
        </NavigationContainer>
    );
}

export default Nav;